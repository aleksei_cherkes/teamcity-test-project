package patches.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.ScriptBuildStep
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_2.ui.*

/*
This patch script was generated by TeamCity on settings change in UI.
To apply the patch, change the buildType with id = 'Build2'
accordingly, and delete the patch script.
*/
changeBuildType(RelativeId("Build2")) {
    expectSteps {
        script {
            id = "RUNNER_4"
            scriptContent = """echo "HW2 -- NEW VERSION""""
        }
    }
    steps {
        update<ScriptBuildStep>(0) {
            id = "RUNNER_4"
            scriptContent = """
                echo "HW2 -- NEW VERSION"
                exit 1
            """.trimIndent()
        }
        insert(1) {
            script {
                name = "MyNewStep"
                id = "RUNNER_2"
                scriptContent = """echo "My command""""
            }
        }
    }
}
