import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, emplates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2019.1"

project {
    defaultTemplate = RelativeId("MyTemplate")

    buildType(Build2)
    buildType(CompositeBuild)
    buildType(Build)

    template(MyTemplate)
}

object Build : BuildType({
    templates(MyTemplate)
    name = "Build1"

    triggers {
        vcs {
            id = "vcsTrigger"
            enabled = false
        }
    }
})

object Build2 : BuildType({
    templates(MyTemplate)
    name = "Build2"

    steps {
        script {
            id = "RUNNER_4"
            scriptContent = """echo "HW2 -- NEW VERSION""""
        }
    }
})

object CompositeBuild : BuildType({
    name = "CompositeBuild"
    description = "My composite build"

    type = BuildTypeSettings.Type.COMPOSITE

    vcs {
        showDependenciesChanges = true
    }

    dependencies {
        snapshot(Build) {
            reuseBuilds = ReuseBuilds.NO
        }
        snapshot(Build2) {
            reuseBuilds = ReuseBuilds.NO
        }
    }
})

object MyTemplate : Template({
    name = "MyTemplate"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            id = "RUNNER_5"
            scriptContent = """echo "HW from %teamcity.build.id% NEW VERSION""""
        }
    }
})
